import 'dart:isolate';

import 'package:flutter/material.dart';

import './MEALS.dart';
import './screens/tabs_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/CategoryMealsScreen.dart';
import './screens/filters_screen.dart';
import './models/Meal.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };
  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  Future<List<Meal>> filterMeals() async {
    // -- the computation --
    return DUMMY_MEALS.where((meal) {
      /// return true if u wanna keep this meal, in new list
      if (_filters['gluten'] && !meal.isGlutenFree) {
        //if filter-gluten is true and meal is not gluten free, don't keep the meal
        return false;
      }
      if (_filters['lactose'] && !meal.isLactoseFree) {
        return false;
      }
      if (_filters['vegan'] && !meal.isVegan) {
        return false;
      }
      if (_filters['vegetarian'] && !meal.isVegetarian) {
        return false;
      }
      return true;
    }).toList();
  }
  void _setFilters(Map<String, bool> filterData) async {
// calling setState, makes the whole app to rebuild
// todo: need a proper way [to manage global-data] / [of state-management]
    setState(() {
      _filters = filterData;
    });
    //TODO: SHOWN AN OVERLAY WITH Text(FILTERING....)
    List<Meal> filteredMeals = await filterMeals(); // trying to do computation in another isolate
    setState(() {
      _availableMeals = filteredMeals;
    });
    //TODO: REMOVE THE OVERLAY
  }

  ///logic to add or remove meals to favoriteMeals
  //takes a meal's mealId, that should be
  // 1. unfavorited -> remove from favoriteMeals
  // 2. favorited -> add from DUMMY_MEALS
  void _toggleFavorite(String mealId) {
    final isMealFavorite =
    _favoriteMeals.indexWhere((meal) => meal.id == mealId);
    if (isMealFavorite >= 0) {
      setState(() {
        _favoriteMeals.removeAt(isMealFavorite);
      });
    } else if (isMealFavorite == -1) {
      setState(() {
        _favoriteMeals.add(
          DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoriteMeals.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            body1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            body2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            title: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            ),
        ),
      ),
//       home: CategoriesScreen(), // implicitly maps to '/' in the route table
      initialRoute: '/', // route on which you wanna your app to start | '/' -> is loaded always first
      routes: { /// to use Navigator.pushNamed, this routes map need to be setUp in a MaterialApp | MaterialPageRoute are generated on the fly, for screens registered here
        '/': (ctx) => TabsScreen(_favoriteMeals),
        CategoryMealsScreen.routeName: (ctx) => CategoryMealsScreen(_availableMeals),
        MealDetailScreen.routeName: (ctx) => MealDetailScreen(_toggleFavorite, _isMealFavorite),
        FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
      },
/// If a route is requested(either by you, or an intent or external-code) that is not specified in routes table (or by [home]), | to handle named-route that's not registered in the routes table
//      onGenerateRoute: (settings) { // can be used when you are generating route-names dynamically during runtime
//        print(settings.arguments); // to find out what argument the Navigator has
//         if (settings.name == '/meal-detail') {
//           return ...;
//         } else if (settings.name == '/something-else') {
//           return ...;
//         }
//         return MaterialPageRoute(builder: (ctx) => CategoriesScreen(),);
//      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => Text('404 Page not exists'),
        );
      },
    );
  }
}