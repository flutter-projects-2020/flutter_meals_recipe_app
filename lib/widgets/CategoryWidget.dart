import 'package:flutter/material.dart';

import '../screens/CategoryMealsScreen.dart';

class CategoryWidget extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  CategoryWidget(this.id, this.title, this.color);
/// meals kha show hoga, its obvious ek meals Screen pe
  void showMeals(BuildContext ctx) {
    Navigator.of(ctx).pushNamed( // In case of using named-routes, we can't pass data to Screen's constructors from here
      CategoryMealsScreen.routeName,
      arguments: { /// To pass data to a new Screen
        'id': id, //output Meals 'corresponding to' this Category id | corresponding to -> contain this Category's id, in its CategoriesIds list
        'title': title,
      },
    );
  }
// 'agle screen' pe 'whi sb meal' 'show krna', jo 'es category ke hain'
//
// agle screen -> MealsScreen
// whi sb meal -> (from a list of available Meals)
// show krna -> Make a MealWidget out of that Meal
// es category ke hain -> (Meal shown contains this categoryId)
// Breakdown tells, Go to MealsScreen, form MealWidgets out of those Meal that contain this category's id in their List-of-CatergoriesIds
                                            // in a list filter out those Meals from MEALS
/* List_to_form_MealWidgets = MEALS.where(
          (meal) => meal.categoriesIds.contain(id)
          ).toList()
*/


  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showMeals(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.title,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [ // colors to transition
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft, // coordinate where this gradient starts
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
