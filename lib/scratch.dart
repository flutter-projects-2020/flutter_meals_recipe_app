import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/route2/route3/route4',
      routes: {
        '/' : (c) => Route1(),
        '/route2' : (c) => Route2(),
        '/route2/route3' : (c) => Route3(),
        '/route2/route3/route4' : (c) => Route4(),
      },
      onGenerateRoute: (s) => MaterialPageRoute(builder: (c) => OnGenerateRoute()) ,
    );
  }
}

class Route1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text('Route1');
  }
}
class Route2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text('Route2');
  }
}
class Route3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text('Route3');
  }
}
class Route4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text('Route4');
  }
}
class OnGenerateRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text('OnGenerateRoute');
  }
}
