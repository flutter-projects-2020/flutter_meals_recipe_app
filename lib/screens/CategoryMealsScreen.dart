import 'package:flutter/material.dart';

import '../widgets/MealWidget.dart';
import '../models/Meal.dart';

class CategoryMealsScreen extends StatefulWidget {
  static const routeName = '/category-meals';

  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals); // MealsScreen(DUMMY_MEALS),

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String categoryTitle;
  List<Meal> displayedMeals;
  var _loadedInitData = false;

  @override
  void initState() {
    // ...
    super.initState();
  }
// 'jis Meal' me 'ye categoryId h' , whi 'show krna h'
/* show krna h -> form MealWidget => ye hoga from a List of Meals
  List of Meals will contain those Meals 'from' MEAL which meet 'some condn'  | Now what is the condn
  // jis/from -> filter out => MEALS.where((m) => m.categoriesIds.contain(categoryId)).toList();
  // 'ye categoryId h' -> contain categoryID => contain hoga ek list me
* */
  @override
  void didChangeDependencies() {
///here the work is done that needs to be done only 1 time ever when the state initializes, but not done in setState bcoz 'context' was not there
    if (!_loadedInitData) {
      final routeArgs = // to extract data passed to this screen
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];

      final categoryId = routeArgs['id']; // make MealWidgets out of those MEALS whose List of CategoryIds contains this Category id

      displayedMeals = widget.availableMeals // In a new List, filter out those Meal
          .where(
              (meal) => meal.categoriesIds.contains(categoryId)
          ).toList();

      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeMeal(String mealId) { //need to pass this function pointer to the class from which it will be executed
    setState(() {
      displayedMeals.removeWhere((meal) => meal.id == mealId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealWidget(
            id: displayedMeals[index].id,
            title: displayedMeals[index].title,
            imageUrl: displayedMeals[index].imageUrl,
            duration: displayedMeals[index].duration,
            affordability: displayedMeals[index].affordability,
            complexity: displayedMeals[index].complexity,
//            removeItem: _removeMeal,
          );
        },
        itemCount: displayedMeals.length,
      ),
    );
  }
}
