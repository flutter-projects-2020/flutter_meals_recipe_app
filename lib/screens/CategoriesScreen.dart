import 'package:flutter/material.dart';

import '../CATEGORIES.dart';
import '../widgets/CategoryWidget.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_CATEGORIES
          .map(
            (ct) => CategoryWidget(
                  ct.id,
                  ct.title,
                  ct.color,
                ),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2, // w = 1.5 * h
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}
