import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';
import './favorites_screen.dart';
import './CategoriesScreen.dart';
import '../models/Meal.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favoriteMeals;
  TabsScreen(this.favoriteMeals);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, dynamic>> _views; // List<Map<String, Object>> _views;
  int _selectedView = 0;

  @override
  void initState() {
    super.initState();
    _views = [
      {//0
        'body': CategoriesScreen(),
        'title': 'Categories',
      },
      {//1
        'body': FavoritesScreen(widget.favoriteMeals), // using a property(widget) to initialize another property(views)
        'title': 'Your Favorite',
      },
    ];
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_views[_selectedView]['title']),
      ),
      drawer: MainDrawer(),
      body: _views[_selectedView]['body'],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedView, /// required, I have to keep this value in the State | BottomNavigationBar doesn't manages its state internally
        onTap: (int index) { // Controller in case of BottomNavigationBar given by us
          setState(() {
            _selectedView = index;
          });
        },
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).accentColor,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.category),
            title: Text('Categories'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.star),
            title: Text('Favorites'),
          ),
        ],
      ),
    );
  }
}
