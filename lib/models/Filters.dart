class Filters {
  final gluten;
  final vegan;
  final vegetarian;
  final lactose;
  Filters({
    this.gluten = false,
    this.lactose = false,
    this.vegan = false,
    this.vegetarian = false});

}